import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./users/login/login.component";
import {RegisterComponent} from "./users/register/register.component";
import {HomeComponent} from "./home/home.component";
import {GameComponent} from "./game/game.component";
import {AuthGuardService} from "./auth/auth-guard.service";
import {UpgradesComponent} from "./admin/upgrades/upgrades.component";
import {AuthAdminGuardService} from "./auth/auth-admin-guard.service";
import {UsersComponent} from "./admin/users/users.component";
import {AchievementsComponent} from "./admin/achievements/achievements.component";
import {AchievementsGameComponent} from "./game/achievements-game/achievements-game.component";


const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },{
    path: "register",
    component: RegisterComponent
  },{
    path: "game",
    component: GameComponent,
    canActivate: [AuthGuardService]
  },{
    path: "game/achievements",
    component: AchievementsGameComponent,
    canActivate: [AuthGuardService]
  },{
    path: "admin/upgrades",
    component: UpgradesComponent,
    canActivate: [AuthAdminGuardService]
  },{
    path: "admin/users",
    component: UsersComponent,
    canActivate: [AuthAdminGuardService]
  },{
    path: "admin/achievements",
    component: AchievementsComponent,
    canActivate: [AuthAdminGuardService]
  },{
    path: "",
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
