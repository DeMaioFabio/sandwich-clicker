import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {JWTService} from "../util/jwt.service";

@Injectable({
  providedIn: 'root'
})
export class AuthAdminGuardService implements CanActivate{

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.jwt.isAdmin())
      return true;
    return false;
  }

  constructor(private jwt: JWTService) { }
}
