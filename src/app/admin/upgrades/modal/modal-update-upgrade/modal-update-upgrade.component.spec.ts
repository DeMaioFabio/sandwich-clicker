import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUpdateUpgradeComponent } from './modal-update-upgrade.component';

describe('ModalUpdateUpgradeComponent', () => {
  let component: ModalUpdateUpgradeComponent;
  let fixture: ComponentFixture<ModalUpdateUpgradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalUpdateUpgradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUpdateUpgradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
