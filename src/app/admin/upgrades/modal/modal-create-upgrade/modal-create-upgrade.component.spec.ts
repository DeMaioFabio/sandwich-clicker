import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreateUpgradeComponent } from './modal-create-upgrade.component';

describe('ModalCreateUpgradeComponent', () => {
  let component: ModalCreateUpgradeComponent;
  let fixture: ComponentFixture<ModalCreateUpgradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCreateUpgradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreateUpgradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
