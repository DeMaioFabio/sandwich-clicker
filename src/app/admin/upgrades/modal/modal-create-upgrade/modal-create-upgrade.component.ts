import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Upgrade} from "../../../../models/upgrade";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-modal-create-upgrade',
  templateUrl: './modal-create-upgrade.component.html',
  styleUrls: ['./modal-create-upgrade.component.css']
})
export class ModalCreateUpgradeComponent implements OnInit {

  ngOnInit() {
  }

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  public readonly REF_NAME: string = "name";
  public readonly REF_VALUE: string = "value";
  public readonly REF_DESCRIPTION: string = "description";
  public readonly REF_ICON: string = "icon";

  private upgrade: Upgrade;

  form: FormGroup = this.fb.group({
    name: this.fb.control('', Validators.required),
    value: this.fb.control('', Validators.required),
    description: this.fb.control('', Validators.required),
    icon: this.fb.control('', Validators.required),
  });

  constructor(public fb: FormBuilder, public modal: NgbActiveModal) {}

  buildUpgrade(){
    this.upgrade = {
      name: this.form.get(this.REF_NAME).value,
      value: this.form.get(this.REF_VALUE).value,
      description: this.form.get(this.REF_DESCRIPTION).value,
      icon: this.form.get(this.REF_ICON).value
    };

    this.passEntry.emit(this.upgrade);
  }
}
