import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRemoveUpgradeComponent } from './modal-remove-upgrade.component';

describe('ModalRemoveUpgradeComponent', () => {
  let component: ModalRemoveUpgradeComponent;
  let fixture: ComponentFixture<ModalRemoveUpgradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRemoveUpgradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRemoveUpgradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
