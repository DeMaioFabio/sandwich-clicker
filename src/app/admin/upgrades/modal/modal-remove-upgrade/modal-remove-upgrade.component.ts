import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-modal-remove-upgrade',
  templateUrl: './modal-remove-upgrade.component.html',
  styleUrls: ['./modal-remove-upgrade.component.css']
})
export class ModalRemoveUpgradeComponent implements OnInit {

  ngOnInit() {
  }

  nameUpgrade: string;

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor(public modal: NgbActiveModal) {}

  clickYes() {
    this.passEntry.emit(true);
  }

  clickNo(){
    this.passEntry.emit(false);
  }
}
