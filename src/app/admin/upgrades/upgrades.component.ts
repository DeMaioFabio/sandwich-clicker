import {Component, OnDestroy, OnInit} from '@angular/core';
import {Upgrade} from "../../models/upgrade";
import {UpgradesService} from "../../httpClientServices/upgrades.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SubscriptionManagerService} from "../../util/subscription-manager.service";
import {ModalRemoveUpgradeComponent} from "./modal/modal-remove-upgrade/modal-remove-upgrade.component";
import {ModalCreateUpgradeComponent} from "./modal/modal-create-upgrade/modal-create-upgrade.component";
import {ModalUpdateUpgradeComponent} from "./modal/modal-update-upgrade/modal-update-upgrade.component";

export enum FilterTypeUpgrade{
  ID = "idUpgrade",
  NAME = "name",
  VALUE = "value"
}

@Component({
  selector: 'app-upgrades',
  templateUrl: './upgrades.component.html',
  styleUrls: ['./upgrades.component.css']
})
export class UpgradesComponent implements OnInit, OnDestroy {

  upgrades: Upgrade[] = [];
  filter: FilterTypeUpgrade = FilterTypeUpgrade.ID;

  constructor(private _modalService: NgbModal, private upgradeService:UpgradesService, private subManager: SubscriptionManagerService) { }

  ngOnInit() {
    this.loadUpgrades();
  }

  ngOnDestroy(): void {
    this.subManager.destroySub();
  }

  private loadUpgrades() {
    this.subManager.addSub(this.upgradeService.getAll().subscribe(upgrades => this.upgrades = upgrades));
  }

  remove(upgrade: Upgrade) {
    const modalRef = this._modalService.open(ModalRemoveUpgradeComponent);
    modalRef.componentInstance.nameUpgrade = upgrade.name;
    this.subManager.addSub(modalRef.componentInstance.passEntry.subscribe((receivedEntry) => {
      if(receivedEntry){
        const index = this.upgrades.indexOf(upgrade);
        if (index > -1) {
          this.upgrades.splice(index, 1);
        }
        this.subManager.addSub(this.upgradeService.delete(upgrade.idUpgrade).subscribe());
      }
    }));
  }

  createUpgrade() {
    const modalRef = this._modalService.open(ModalCreateUpgradeComponent);
    this.subManager.addSub(modalRef.componentInstance.passEntry.subscribe(receivedEntry => {
      this.subManager.addSub(this.upgradeService.post(receivedEntry).subscribe(upgrade => {
        this.upgrades.push(upgrade);
      }));
    }));
  }

  update(upgrade: Upgrade) {
    const modalRef = this._modalService.open(ModalUpdateUpgradeComponent);
    modalRef.componentInstance.upgrade = upgrade;
    modalRef.componentInstance.fillInputs();

    this.subManager.addSub(modalRef.componentInstance.passEntry.subscribe(receivedEntry => {
      this.subManager.addSub(this.upgradeService.put(receivedEntry).subscribe(upgradeFromServer => {
        for(let el of this.upgrades){
          if(el.idUpgrade == upgradeFromServer.idUpgrade){
            const index = this.upgrades.indexOf(el);
            if (index > -1) {
              this.upgrades[index] = upgradeFromServer;
            }
          }
        }
      }));
    }));
  }

  updateFilter($event: any) {
    let value = $event.target.value;
    if(value == FilterTypeUpgrade.NAME){
      this.filter = FilterTypeUpgrade.NAME;
    }else if(value == FilterTypeUpgrade.VALUE){
      this.filter = FilterTypeUpgrade.VALUE;
    }else{
      this.filter = FilterTypeUpgrade.ID;
    }
  }
}
