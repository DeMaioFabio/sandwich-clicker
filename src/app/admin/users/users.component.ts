import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from "../../models/user";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SubscriptionManagerService} from "../../util/subscription-manager.service";
import {UsersService} from "../../httpClientServices/users.service";
import {ModalUpdateUserComponent} from "./modal/modal-update-user/modal-update-user.component";

export enum FilterTypeUser{
  ID = "id",
  LASTNAME = "lastName",
  FIRSTNAME = "firstName",
  PSEUDO = "pseudo",
  EMAIL = "email",
  ADMIN = "isAdmin",
  ACTIVATE = "isActivated",
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {

  users: User[];
  filter: FilterTypeUser = FilterTypeUser.ID;

  constructor(public usersService:UsersService, private _modalService: NgbModal, private subManager: SubscriptionManagerService) { }

  ngOnInit() {
    this.loadUsers();
  }

  ngOnDestroy(): void {
    this.subManager.destroySub();
  }

  loadUsers(){
    this.subManager.addSub(this.usersService.getAll().subscribe(users => this.users = users));
  }

  updateFilter($event: any) {
    if($event.target.value == FilterTypeUser.LASTNAME.valueOf()){
      this.filter = FilterTypeUser.LASTNAME;
    }else if($event.target.value == FilterTypeUser.FIRSTNAME.valueOf()){
      this.filter = FilterTypeUser.FIRSTNAME;
    }else if($event.target.value == FilterTypeUser.PSEUDO.valueOf()){
      this.filter = FilterTypeUser.PSEUDO;
    }else if($event.target.value == FilterTypeUser.EMAIL.valueOf()){
      this.filter = FilterTypeUser.EMAIL;
    }else if($event.target.value == FilterTypeUser.ADMIN.valueOf()){
      this.filter = FilterTypeUser.ADMIN;
    }else if($event.target.value == FilterTypeUser.ACTIVATE.valueOf()){
      this.filter = FilterTypeUser.ACTIVATE;
    }else{
      this.filter = FilterTypeUser.ID;
    }
  }

  update(user: User) {
    const modalRef = this._modalService.open(ModalUpdateUserComponent);
    modalRef.componentInstance.user = user;
    modalRef.componentInstance.fillInputs();

    this.subManager.addSub(modalRef.componentInstance.passEntry.subscribe(receivedEntry => {
      this.subManager.addSub(this.usersService.put(receivedEntry).subscribe(upgradeFromServer => {
        for(let el of this.users){
          if(el.id == upgradeFromServer.id){
            const index = this.users.indexOf(el);
            if (index > -1) {
              this.users[index] = upgradeFromServer;
            }
          }
        }
      }));
    }));
  }
}
