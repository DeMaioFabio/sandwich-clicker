import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from "../../../../models/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-modal-update-user',
  templateUrl: './modal-update-user.component.html',
  styleUrls: ['./modal-update-user.component.css']
})
export class ModalUpdateUserComponent implements OnInit {

  ngOnInit() {
  }

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  public readonly REF_LASTNAME: string = "lastName";
  public readonly REF_FIRSTNAME: string = "firstName";
  public readonly REF_EMAIL: string = "email";
  public readonly REF_ISADMIN: string = "isAdmin";
  public readonly REF_ISACTIVATED: string = "isActivated";

  user: User;

  form: FormGroup = this.fb.group({
    lastName: this.fb.control('', Validators.required),
    firstName: this.fb.control('', Validators.required),
    email: this.fb.control('', Validators.required),
    isAdmin: this.fb.control('', [Validators.required, Validators.pattern("[0-1]")]),
    isActivated: this.fb.control('', [Validators.required, Validators.pattern("[0-1]")]),
  });

  constructor(public fb: FormBuilder, public modal: NgbActiveModal) {}

  buildUser(){
    this.user = {
      id: this.user.id,
      lastName: this.form.get(this.REF_LASTNAME).value,
      firstName: this.form.get(this.REF_FIRSTNAME).value,
      pseudo: this.user.pseudo,
      email: this.form.get(this.REF_EMAIL).value,
      password: this.user.password,
      isAdmin: this.form.get(this.REF_ISADMIN).value,
      isActivated: this.form.get(this.REF_ISACTIVATED).value
    };

    this.passEntry.emit(this.user);
  }

  fillInputs() {
    this.form.get(this.REF_LASTNAME).setValue(this.user.lastName);
    this.form.get(this.REF_FIRSTNAME).setValue(this.user.firstName);
    this.form.get(this.REF_EMAIL).setValue(this.user.email);
    this.form.get(this.REF_ISADMIN).setValue(this.user.isAdmin);
    this.form.get(this.REF_ISACTIVATED).setValue(this.user.isActivated);
  }

}
