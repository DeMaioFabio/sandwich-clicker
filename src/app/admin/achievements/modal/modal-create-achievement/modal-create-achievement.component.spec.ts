import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreateAchievementComponent } from './modal-create-achievement.component';

describe('ModalCreateAchievementComponent', () => {
  let component: ModalCreateAchievementComponent;
  let fixture: ComponentFixture<ModalCreateAchievementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCreateAchievementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreateAchievementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
