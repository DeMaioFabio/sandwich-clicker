import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUpdateAchievementComponent } from './modal-update-achievement.component';

describe('ModalUpdateAchievementComponent', () => {
  let component: ModalUpdateAchievementComponent;
  let fixture: ComponentFixture<ModalUpdateAchievementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalUpdateAchievementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalUpdateAchievementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
