import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteAchievementComponent } from './modal-delete-achievement.component';

describe('ModalDeleteAchievementComponent', () => {
  let component: ModalDeleteAchievementComponent;
  let fixture: ComponentFixture<ModalDeleteAchievementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDeleteAchievementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDeleteAchievementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
