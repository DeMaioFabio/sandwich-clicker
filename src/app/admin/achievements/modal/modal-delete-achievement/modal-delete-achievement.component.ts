import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-modal-delete-achievement',
  templateUrl: './modal-delete-achievement.component.html',
  styleUrls: ['./modal-delete-achievement.component.css']
})
export class ModalDeleteAchievementComponent implements OnInit {

  ngOnInit() {
  }

  nameAchievement: string;

  @Output() passEntry: EventEmitter<any> = new EventEmitter();

  constructor(public modal: NgbActiveModal) {}

  clickYes() {
    this.passEntry.emit(true);
  }

  clickNo(){
    this.passEntry.emit(false);
  }

}
