import {Component, OnDestroy, OnInit} from '@angular/core';
import {Achievement} from "../../models/achievement";
import {AchievementsService} from "../../httpClientServices/achievements.service";
import {SubscriptionManagerService} from "../../util/subscription-manager.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ModalDeleteAchievementComponent} from "./modal/modal-delete-achievement/modal-delete-achievement.component";
import {ModalCreateAchievementComponent} from "./modal/modal-create-achievement/modal-create-achievement.component";
import {ModalUpdateAchievementComponent} from "./modal/modal-update-achievement/modal-update-achievement.component";

export enum FilterTypeAchievement{
  ID = "idAchievement",
  NAME = "name",
  VALUE = "value"
}

@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.css']
})
export class AchievementsComponent implements OnInit, OnDestroy {

  public achievements: Achievement[] = [];
  public filter: FilterTypeAchievement = FilterTypeAchievement.ID;

  constructor(private _modalService: NgbModal, private achievementService: AchievementsService, private subManager: SubscriptionManagerService) { }

  ngOnInit(): void {
    this.loadAchievements();
  }

  loadAchievements() {
    this.subManager.addSub(this.achievementService.getAll().subscribe(response => this.achievements = response));
  }

  ngOnDestroy(): void {
    this.subManager.destroySub();
  }

  createAchievement() {
    const modalRef = this._modalService.open(ModalCreateAchievementComponent);
    this.subManager.addSub(modalRef.componentInstance.passEntry.subscribe(receivedEntry => {
      this.subManager.addSub(this.achievementService.post(receivedEntry).subscribe(upgrade => {
        this.achievements.push(upgrade);
      }));
    }));
  }

  update(achievement: Achievement) {
    const modalRef = this._modalService.open(ModalUpdateAchievementComponent);
    modalRef.componentInstance.achievement = achievement;
    modalRef.componentInstance.fillInputs();

    this.subManager.addSub(modalRef.componentInstance.passEntry.subscribe(receivedEntry => {
      this.subManager.addSub(this.achievementService.put(receivedEntry).subscribe(achievementFromServer => {
        for(let el of this.achievements){
          if(el.idAchievement == achievementFromServer.idAchievement){
            const index = this.achievements.indexOf(el);
            if (index > -1) {
              this.achievements[index] = achievementFromServer;
            }
          }
        }
      }));
    }));
  }

  remove(achievement: Achievement) {
    const modalRef = this._modalService.open(ModalDeleteAchievementComponent);
    modalRef.componentInstance.nameAchievement = achievement.name;
    this.subManager.addSub(modalRef.componentInstance.passEntry.subscribe((receivedEntry) => {
      if(receivedEntry){
        const index = this.achievements.indexOf(achievement);
        if (index > -1) {
          this.achievements.splice(index, 1);
        }
        this.subManager.addSub(this.achievementService.delete(achievement.idAchievement).subscribe());
      }
    }));
  }

  updateFilter($event: any) {
    let value = $event.target.value;
    if(value == FilterTypeAchievement.NAME){
      this.filter = FilterTypeAchievement.NAME;
    }else if(value == FilterTypeAchievement.VALUE){
      this.filter = FilterTypeAchievement.VALUE;
    }else{
      this.filter = FilterTypeAchievement.ID;
    }
  }
}
