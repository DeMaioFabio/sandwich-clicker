import { Pipe, PipeTransform } from '@angular/core';
import {Upgrade} from "../../models/upgrade";
import {FilterTypeUpgrade} from "../upgrades/upgrades.component";

@Pipe({
  name: 'sortUpgrade'
})
export class SortUpgradePipe implements PipeTransform {

  transform(value: Upgrade[], filter: FilterTypeUpgrade): Upgrade[] {
    if (value.length != 0) {
      if (filter == FilterTypeUpgrade.NAME) {
        value.sort((a, b) => {
          if (a.name < b.name) {
            return -1;
          } else if (a.name > b.name) {
            return 1;
          }
          return 0
        });
      } else if (filter == FilterTypeUpgrade.VALUE) {
        value.sort((a, b) => {
          if (a.value < b.value) {
            return -1;
          } else if (a.value > b.value) {
            return 1;
          }
          return 0
        });
      } else {
        value.sort((a, b) => {
          if (a.idUpgrade < b.idUpgrade) {
            return -1;
          } else if (a.idUpgrade > b.idUpgrade) {
            return 1;
          }
          return 0
        });
      }
    }
    return value;
  }
}
