import { Pipe, PipeTransform } from '@angular/core';
import {FilterTypeAchievement} from "../achievements/achievements.component";
import {Achievement} from "../../models/achievement";

@Pipe({
  name: 'sortAchievement'
})
export class SortAchievementPipe implements PipeTransform {

  transform(value: Achievement[], filter: FilterTypeAchievement): Achievement[] {

    if (value.length != 0) {
      if (filter == FilterTypeAchievement.NAME) {
        value.sort((a, b) => {
          if (a.name.toUpperCase() < b.name.toUpperCase()) {
            return -1;
          } else if (a.name.toUpperCase() > b.name.toUpperCase()) {
            return 1;
          }
          return 0
        });
      } else if (filter == FilterTypeAchievement.VALUE) {
        value.sort((a, b) => {
          if (a.value < b.value) {
            return -1;
          } else if (a.value > b.value) {
            return 1;
          }
          return 0
        });
      } else {
        value.sort((a, b) => {
          if (a.idAchievement < b.idAchievement) {
            return -1;
          } else if (a.idAchievement > b.idAchievement) {
            return 1;
          }
          return 0
        });
      }
    }
    return value;
  }
}
