import {Pipe, PipeTransform} from '@angular/core';
import {User} from "../../models/user";
import {FilterTypeUser} from "../users/users.component";

@Pipe({
  name: 'sortUser'
})
export class SortUserPipe implements PipeTransform {

  transform(value: User[], filter: FilterTypeUser): User[] {
    if(value != undefined){
      let filt = filter.valueOf();
      if(filter == FilterTypeUser.LASTNAME || filter == FilterTypeUser.FIRSTNAME || filter == FilterTypeUser.EMAIL || filter == FilterTypeUser.PSEUDO){
        value.sort((a, b) => {
          if(a[filt].toLowerCase() < b[filt].toLowerCase()){
            return -1;
          }else if(a[filt].toLowerCase() > b[filt].toLowerCase()){
            return 1;
          }else{
            return 0;
          }
        });
      }else{
        value.sort((a, b) => {
          if(a[filt] < b[filt]){
            return -1;
          }else if(a[filt] > b[filt]){
            return 1;
          }else{
            return 0;
          }
        });
      }
    }
    return value;
  }
}
