import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JWTService {

  private TOKEN: string = "token";

  constructor() { }

  public hasToken(): boolean{
    return window.localStorage.getItem(this.TOKEN) != null;
  }

  public isAdmin(): boolean{
    if(this.hasToken()){
      return this.decodeToken().is_admin == true;
    }
    return false;
  }

  public logout(){
    localStorage.removeItem(this.TOKEN);
  }

  public saveToken(token: string) {
    localStorage.setItem(this.TOKEN, token);
  }

  public getIdUser(){
    return this.decodeToken().id_user;
  }

  private decodeToken(){
    const token = localStorage.getItem(this.TOKEN);

    let jwtDecode = require('jwt-decode');
    const tokenDecoded = jwtDecode(token);

    return tokenDecoded;
  }
}
