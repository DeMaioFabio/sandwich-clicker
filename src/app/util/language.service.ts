import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  private LANGUAGE: string = "language";

  constructor() { }

  setLanguage(language: string){
    if(window.localStorage.getItem(this.LANGUAGE) != language){
      window.localStorage.setItem(this.LANGUAGE, language);
    }
  }
}
