import { Injectable } from '@angular/core';
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SubscriptionManagerService {

  private subscriptions: Subscription[] = [];

  constructor() { }

  public addSub(sub: Subscription){
    this.subscriptions.push(sub);
  }

  public destroySub(){
    for(let sub of this.subscriptions){
      sub.unsubscribe();
    }
  }
}
