import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Statistic} from "../models/statistic";

@Injectable({
  providedIn: 'root'
})
export class StatisticService {

  private URL_API: string = "/api/statistic";

  constructor(private http: HttpClient) { }

  get(id: number):Observable<any> {
    return this.http.get(this.URL_API+"/"+id, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }

  put(stat : Statistic){
    return this.http.put(this.URL_API, stat, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }
}
