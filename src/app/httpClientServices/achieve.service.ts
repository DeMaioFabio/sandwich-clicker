import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AchieveService {

  private URL_API: string = "/api/achieve";

  constructor(private http:HttpClient) { }

  getAll(id: number):Observable<any> {
    return this.http.get(this.URL_API+"/"+id, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }

  post(achieve: any): Observable<any> {
    return this.http.post(this.URL_API, achieve, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }
}
