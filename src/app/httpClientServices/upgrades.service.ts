import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UpgradesService {

  private URL_API: string = "/api/upgrade";

  constructor(private http:HttpClient) { }

  getAll(): Observable<any>{
    return this.http.get(this.URL_API, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }

  delete(id:number): Observable<any>{
    return this.http.delete(this.URL_API+"/"+id, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }

  post(upgrade: any): Observable<any> {
    return this.http.post(this.URL_API, upgrade, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }

  put(upgrade: any):Observable<any> {
    return this.http.put(this.URL_API, upgrade, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }
}
