import { Injectable } from '@angular/core';
import {User} from "../models/user";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Login} from "../models/login";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private static URL_API: string = "/api/user";

  constructor(public http: HttpClient) {
  }

  post(user: User): Observable<User> {
    return this.http.post<User>(UsersService.URL_API, user);
  }

  get(login: Login): Observable<any> {
    return this.http.get(UsersService.URL_API+"/"+login.pseudo+"/"+login.password);
  }

  getAll():Observable<any> {
    return this.http.get(UsersService.URL_API, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }

  put(user: any):Observable<any> {
    return this.http.put(UsersService.URL_API, user, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    })
  }
}
