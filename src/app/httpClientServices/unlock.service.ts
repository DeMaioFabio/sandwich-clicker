import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Unlock} from "../models/unlock";

@Injectable({
  providedIn: 'root'
})
export class UnlockService {

  private URL_API: string = "/api/unlock";

  constructor(private http: HttpClient) { }

  getAll(id: number):Observable<any> {
    return this.http.get(this.URL_API+"/"+id, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }

  put(unlock: Unlock){
    return this.http.put(this.URL_API, unlock, {
      headers: {
        'Content-Type':'application/json; charset=utf-8',
        'Authorization':'bearer '+localStorage.getItem("token")
      }
    });
  }
}
