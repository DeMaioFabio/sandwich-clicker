import { TestBed } from '@angular/core/testing';

import { UnlockService } from './unlock.service';

describe('UnlockService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnlockService = TestBed.get(UnlockService);
    expect(service).toBeTruthy();
  });
});
