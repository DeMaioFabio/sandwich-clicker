import {Component, enableProdMode, OnDestroy, OnInit, TRANSLATIONS, TRANSLATIONS_FORMAT} from '@angular/core';
import {AccountCreatedService} from "./users/account-created.service";
import {Subscription} from "rxjs";
import {User} from "./models/user";
import {UsersService} from "./httpClientServices/users.service";
import {LoginRequestService} from "./users/login-request.service";
import {Login} from "./models/login";
import {SubscriptionManagerService} from "./util/subscription-manager.service";
import { JWTService } from './util/jwt.service';
import {LanguageService} from "./util/language.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{

  private subAccountCreated: Subscription;
  private subPost: Subscription;

  private subLoginRequest: Subscription;
  private subLoginRequestResponse: Subscription;

  constructor(public languageService: LanguageService, private usersService: UsersService, private accountCreatedService: AccountCreatedService, private loginRequestService: LoginRequestService, private subManager: SubscriptionManagerService, public jwt: JWTService){ }

  ngOnInit(): void {
    this.listenUserCreated();
  }

  private listenUserCreated(): void{
    this.subAccountCreated = this.accountCreatedService.$accountCreated.subscribe(newAccount => this.createAccount(newAccount));
    this.subLoginRequest = this.loginRequestService.$loginRequest.subscribe(login => this.requestLogin(login));
  }

  ngOnDestroy(): void {
    this.subAccountCreated.unsubscribe();
    this.subPost.unsubscribe();
    this.subLoginRequest.unsubscribe();
    this.subLoginRequestResponse.unsubscribe();
  }

  private createAccount(user: User){
    this.subPost = this.usersService.post(user).subscribe(userFromServer => this.accountCreatedService.notifyResponseFromServer(userFromServer));
  }

  private requestLogin(login: Login) {
    this.subLoginRequestResponse = this.usersService.get(login).subscribe(response => this.loginRequestService.notifyResponseFromServer(response));
  }
}
