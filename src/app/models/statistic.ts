export class Statistic {

  idUser: number = 0;
  nbSandwich: number = 0;
  highestNbSandwich: number = 0;
  highestClickValue: number = 0;
  highestSecondValue: number = 0;
  nbSandwichGot: number = 0;
  nbClicks: number = 0;
  timePlayed: number = 0;
}
