export class Achievement {

  idAchievement?: number;
  name: string;
  description: string;
  value: number;
  icon: string;
}
