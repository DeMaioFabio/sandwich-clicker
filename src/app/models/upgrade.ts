export class Upgrade {

  idUpgrade?: number;
  value: number;
  name: string;
  description: string;
  icon: string;
}
