export class User {

  id?: number;
  lastName: string;
  firstName: string;
  pseudo: string;
  email: string;
  password: string;
  isAdmin: number;
  isActivated: number;

}
