import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './users/register/register.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { LoginComponent } from './users/login/login.component';
import { GameComponent } from './game/game.component';
import {HttpClientModule} from "@angular/common/http";
import {UpgradesComponent} from './admin/upgrades/upgrades.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { SortUpgradePipe } from './admin/pipe/sort-upgrade.pipe';
import {UsersComponent} from './admin/users/users.component';
import { SortUserPipe } from './admin/pipe/sort-user.pipe';
import {SortUpgradeGamePipe} from "./game/pipe/sort-upgrade-game.pipe";
import { DisplayTimerPipe } from './game/pipe/display-timer.pipe';
import {AchievementsComponent} from './admin/achievements/achievements.component';
import { SortAchievementPipe } from './admin/pipe/sort-achievement.pipe';
import { AchievementsGameComponent } from './game/achievements-game/achievements-game.component';
import { ModalRemoveUpgradeComponent } from './admin/upgrades/modal/modal-remove-upgrade/modal-remove-upgrade.component';
import { ModalCreateUpgradeComponent } from './admin/upgrades/modal/modal-create-upgrade/modal-create-upgrade.component';
import { ModalUpdateUpgradeComponent } from './admin/upgrades/modal/modal-update-upgrade/modal-update-upgrade.component';
import { ModalDeleteAchievementComponent } from './admin/achievements/modal/modal-delete-achievement/modal-delete-achievement.component';
import { ModalCreateAchievementComponent } from './admin/achievements/modal/modal-create-achievement/modal-create-achievement.component';
import { ModalUpdateAchievementComponent } from './admin/achievements/modal/modal-update-achievement/modal-update-achievement.component';
import { ModalUpdateUserComponent } from './admin/users/modal/modal-update-user/modal-update-user.component';

  @NgModule({
    declarations: [
      AppComponent,
      HomeComponent,
      RegisterComponent,
      LoginComponent,
      GameComponent,
      UpgradesComponent,
      SortUpgradePipe,
      UsersComponent,
      SortUserPipe,
      SortUpgradeGamePipe,
      DisplayTimerPipe,
      AchievementsComponent,
      SortAchievementPipe,
      AchievementsGameComponent,
      ModalRemoveUpgradeComponent,
      ModalCreateUpgradeComponent,
      ModalUpdateUpgradeComponent,
      ModalDeleteAchievementComponent,
      ModalCreateAchievementComponent,
      ModalUpdateAchievementComponent,
      ModalUpdateUserComponent
    ],
    entryComponents:[
      ModalRemoveUpgradeComponent,
      ModalCreateUpgradeComponent,
      ModalUpdateUpgradeComponent,
      ModalDeleteAchievementComponent,
      ModalCreateAchievementComponent,
      ModalUpdateAchievementComponent,
      ModalUpdateUserComponent
    ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      ReactiveFormsModule,
      FormsModule,
      HttpClientModule,
      NgbModule
    ],
      providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
