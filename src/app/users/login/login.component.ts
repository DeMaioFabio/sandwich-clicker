import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginRequestService} from "../login-request.service";
import {Login} from "../../models/login";
import {SubscriptionManagerService} from "../../util/subscription-manager.service";
import { JWTService } from 'src/app/util/jwt.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  public readonly REF_PSEUDO: string = "pseudo";
  public readonly REF_PASSWORD: string = "password";

  form: FormGroup = this.fb.group({
    pseudo: this.fb.control('', Validators.required),
    password: this.fb.control('', Validators.required)
  });

  public errorMessage: string;
  public succesMessage: string;

  constructor(public fb: FormBuilder, private loginRequest: LoginRequestService, private subManager: SubscriptionManagerService, private jwt: JWTService) { }

  private listenLoginFromServer(): void{
    this.subManager.addSub(this.loginRequest.$loginRequestResponseFromServer.subscribe(tokenFromServer => this.responseFromServer(tokenFromServer)));
  }

  ngOnInit() {
    this.listenLoginFromServer();
  }

  ngOnDestroy(){
    this.subManager.destroySub();
  }

  submitLoginForm() {
    this.loginRequest.notify(this.buildLogin());
    this.form.reset();
  }

  buildLogin(): Login{
    return {
      "pseudo": this.form.get(this.REF_PSEUDO).value,
      "password": this.form.get(this.REF_PASSWORD).value
    }
  }

  private responseFromServer(response: any) {
    this.errorMessage = "";
    this.succesMessage = "";

    if(response == null || response == "") {
      this.errorMessage = "Le pseudo ou le mot de passe est incorrect !";
      return;
    }

    if(response.token == " "){
      this.errorMessage = "Votre compte a été désactivé par un administrateur !";
      this.succesMessage = "<br/>Veuillez nous contacter pour résoudre le problème.";
      return;
    }

    this.jwt.saveToken(response.token);
    this.succesMessage = "Vous êtes maintenant connecté !";
    return;
  }
}
