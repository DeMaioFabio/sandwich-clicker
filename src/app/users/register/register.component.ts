import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../models/user";
import {AccountCreatedService} from "../account-created.service";
import {SubscriptionManagerService} from "../../util/subscription-manager.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

  readonly REF_LAST_NAME: string = "lastName";
  readonly REF_FIRST_NAME: string = "firstName";
  readonly REF_PSEUDO: string = "pseudo";
  readonly REF_EMAIL: string = "email";
  readonly REF_PASSWORD: string = "password";
  readonly REF_REPEAT_PASSWORD: string = "repeatPassword";

  readonly EMAIL_PATTERN = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";

  public errorMessage: string;
  public succesMessage: string;

  constructor(private fb:FormBuilder, private accountCreatedService:AccountCreatedService, private subManager: SubscriptionManagerService) { }

  ngOnInit() {
    this.listenUserFromServer();
  }

  private listenUserFromServer(): void{
    this.subManager.addSub(this.accountCreatedService.$accountCreatedResponseFromServer.subscribe(userFromServer => this.responseFromServer(userFromServer)));
  }

  ngOnDestroy(){
    this.subManager.destroySub();
  }

  form: FormGroup = this.fb.group({
    lastName: this.fb.control('', Validators.required),
    firstName: this.fb.control('', Validators.required),
    pseudo: this.fb.control('', Validators.required),
    email: this.fb.control('', [Validators.required, Validators.pattern(this.EMAIL_PATTERN)]),
    password: this.fb.control('', Validators.required),
    repeatPassword: this.fb.control('', Validators.required)
  });

  submitForm() {
    if(this.validForm()){
      this.accountCreatedService.notify(this.buildUser());
      this.form.reset();
    }
  }

  validForm():boolean{
    this.errorMessage = "";

    if(this.form.get(this.REF_PSEUDO).value.length < 8) {
      this.errorMessage = "Pseudo trop petit !";
      return false;
    }

    if(this.form.get(this.REF_PASSWORD).value.length < 8){
      this.errorMessage = "Mot de passe trop petit !";
      return false;
    }

    if(this.form.get(this.REF_PASSWORD).value != this.form.get(this.REF_REPEAT_PASSWORD).value){
      this.errorMessage = "Les mots de passes ne sont pas les mêmes !";
      return false;
    }

    return true;
  }

  buildUser():User {
    return {
      lastName: this.form.get(this.REF_LAST_NAME).value,
      firstName: this.form.get(this.REF_FIRST_NAME).value,
      pseudo: this.form.get(this.REF_PSEUDO).value,
      email: this.form.get(this.REF_EMAIL).value,
      password: this.form.get(this.REF_PASSWORD).value,
      isAdmin: 0,
      isActivated: 1
    };
  }

  responseFromServer(user: User): void{
    if(user == null){
      this.errorMessage = "Ce pseudo existe déjà";
    }else{
      this.succesMessage = "Votre inscription s'est déroulée avec succès !";
    }
  }
}
