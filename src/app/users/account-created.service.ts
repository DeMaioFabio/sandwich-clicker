import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class AccountCreatedService {

  private subject:Subject<User> = new Subject<User>();
  public $accountCreated:Observable<User> = this.subject.asObservable();

  private subjectResponseFromServer:Subject<User>= new Subject<User>();
  public $accountCreatedResponseFromServer:Observable<User> = this.subjectResponseFromServer.asObservable();

  constructor() { }

  notify(user:User): void{
    this.subject.next(user);
  }

  notifyResponseFromServer(user: User): void{
    this.subjectResponseFromServer.next(user);
  }
}
