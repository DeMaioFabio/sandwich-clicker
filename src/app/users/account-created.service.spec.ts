import { TestBed } from '@angular/core/testing';

import { AccountCreatedService } from './account-created.service';

describe('AccountCreatedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountCreatedService = TestBed.get(AccountCreatedService);
    expect(service).toBeTruthy();
  });
});
