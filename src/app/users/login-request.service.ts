import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {Login} from "../models/login";

@Injectable({
  providedIn: 'root'
})
export class LoginRequestService {

  private subject:Subject<{ password: string; pseudo: string }> = new Subject<{ password: string; pseudo: string }>();
  public $loginRequest:Observable<{ password: string; pseudo: string }> = this.subject.asObservable();

  private subjectResponseFromServer:Subject<Login>= new Subject<Login>();
  public $loginRequestResponseFromServer:Observable<Login> = this.subjectResponseFromServer.asObservable();

  constructor() { }

  notify(login: Login): void{
    this.subject.next(login);
  }

  notifyResponseFromServer(response: any): void{
    this.subjectResponseFromServer.next(response);
  }
}
