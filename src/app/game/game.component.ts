import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {Statistic} from "../models/statistic";
import {UpgradesService} from "../httpClientServices/upgrades.service";
import {Upgrade} from "../models/upgrade";
import {StatisticService} from "../httpClientServices/statistic.service";
import {UnlockService} from "../httpClientServices/unlock.service";
import {SubscriptionManagerService} from "../util/subscription-manager.service";
import {AchievementsService} from "../httpClientServices/achievements.service";
import {AchieveService} from "../httpClientServices/achieve.service";
import {Achievement} from "../models/achievement";
import {Achieve} from "../models/achieve";
import {JWTService} from "../util/jwt.service";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit, OnDestroy {

  public stats: Statistic;
  public upgrades: Upgrade[];
  public nbSandwichPerClick: number = 1;
  public nbSandwichPerSecond: number = 1;
  public upgradesBought = new Map<number, number>();
  private currentUpgrade: number;
  public errorMessage: any;
  private intervalEachSecond: NodeJS.Timer;
  private intervalEach30Seconds: NodeJS.Timer;
  private achievements: Achievement[];
  private achieves: Achieve[];

  constructor(private jwt: JWTService, private statisticService: StatisticService, private upgradeService: UpgradesService, private unlockService: UnlockService, private subManager: SubscriptionManagerService, private achievementService: AchievementsService, private achieveService: AchieveService) { }

  ngOnDestroy(): void {
    this.saveGame();

    this.subManager.destroySub();

    clearInterval(this.intervalEachSecond);
    clearInterval(this.intervalEach30Seconds);
  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHander(event) {
    this.saveGame();
  }

  ngOnInit() {
    this.subManager.addSub(this.statisticService.get(this.jwt.getIdUser()).subscribe(response => this.stats = response));
    let prom = new Promise(resolve => {
      this.subManager.addSub(this.upgradeService.getAll().subscribe(response => {
        this.upgrades = response;
        resolve();
      }));
    }).then(value => {
      this.subManager.addSub(this.unlockService.getAll(this.jwt.getIdUser()).subscribe(response => {
        for(let el of response){
          this.upgradesBought.set(el.idUpgrade, el.nbUnlock);
          for(let elem of this.upgrades){
            if(el.idUpgrade == elem.idUpgrade){
              this.nbSandwichPerSecond += (elem.value / 4) * el.nbUnlock;
              this.nbSandwichPerClick += elem.value * el.nbUnlock;
              this.stats.highestSecondValue = this.nbSandwichPerSecond;
              this.stats.highestClickValue = this.nbSandwichPerClick;
            }
          }
        }
      }));
    });

    this.subManager.addSub(this.achievementService.getAll().subscribe(response => this.achievements = response));
    this.subManager.addSub(this.achieveService.getAll(this.jwt.getIdUser()).subscribe(response => this.achieves = response));

    this.intervalEachSecond = setInterval(() => {
      this.autoCollect();
      this.stats.timePlayed += 1;
    }, 1000);

    this.intervalEach30Seconds = setInterval(() => {
      this.autoCollect();
      this.stats.timePlayed += 1;
      this.saveGame();
    }, 30000);
  }

  chooseUpgrade(id: number) {
    for(let el of this.upgrades){
      document.getElementById(el.idUpgrade.toString()).classList.remove("clicked");
    }
    document.getElementById(id.toString()).classList.add("clicked");
    this.currentUpgrade = id;
  }

  clickOnSandwich() {
    this.stats.nbClicks += 1;
    this.stats.nbSandwich += this.nbSandwichPerClick;
    this.stats.nbSandwichGot += this.nbSandwichPerClick;
    if(this.stats.highestNbSandwich < this.stats.nbSandwich){
      this.stats.highestNbSandwich = this.stats.nbSandwich;
    }
    this.checkNewAchievement();
  }

  autoCollect() {
    this.stats.nbSandwich += this.nbSandwichPerSecond;
    this.stats.nbSandwichGot += this.nbSandwichPerSecond;
    if(this.stats.highestNbSandwich < this.stats.nbSandwich){
      this.stats.highestNbSandwich = this.stats.nbSandwich;
    }
    this.checkNewAchievement();
  }

  buyUpgrade() {
    document.getElementById("errorMessage").classList.remove("errorMessage");
    this.errorMessage = "";

    if(this.currentUpgrade == undefined){
      document.getElementById("errorMessage").classList.add("errorMessage");
      this.errorMessage = "You haven't selected an upgrade !";
      return;
    }

    for(let el of this.upgrades){
      if(this.currentUpgrade == el.idUpgrade){
        if(this.stats.nbSandwich < el.value * 100){
          document.getElementById("errorMessage").classList.add("errorMessage");
          this.errorMessage = "You haven't enough sandwiches !";
          return;
        }
        this.stats.nbSandwich -= el.value * 100;
        this.nbSandwichPerClick += el.value;
        this.nbSandwichPerSecond += el.value / 4;

        if(this.stats.highestClickValue < this.nbSandwichPerClick){
          this.stats.highestClickValue = this.nbSandwichPerClick;
        }
        if(this.stats.highestSecondValue < this.nbSandwichPerSecond){
          this.stats.highestSecondValue = this.nbSandwichPerSecond;
        }
      }
    }

    if(this.upgradesBought.has(this.currentUpgrade)){
      this.upgradesBought.set(this.currentUpgrade, this.upgradesBought.get(this.currentUpgrade) + 1);
    }else{
      this.upgradesBought.set(this.currentUpgrade, 1);
    }
  }

  private saveGame() {
    this.subManager.addSub(this.statisticService.put(this.stats).subscribe());
    if(this.stats.idUser != 0){
      this.upgradesBought.forEach((key, value) => {
        let prom = new Promise(resolve => {
          this.subManager.addSub(this.unlockService.put(
            {
              idUser: this.stats.idUser,
              idUpgrade: value,
              nbUnlock: key
            }
          ).subscribe());
          resolve();
        }).then(() => {
          if(key == 0){
            this.upgradesBought.delete(value);
          }
        });
      });
    }
  }

  sellUpgrade() {
    document.getElementById("errorMessage").classList.remove("errorMessage");
    this.errorMessage = "";

    if(this.currentUpgrade == undefined){
      document.getElementById("errorMessage").classList.add("errorMessage");
      this.errorMessage = "You haven't selected an upgrade !";
      return;
    }

    if(this.upgradesBought.has(this.currentUpgrade)){
      if(this.upgradesBought.get(this.currentUpgrade) > 0){
        this.upgradesBought.set(this.currentUpgrade, this.upgradesBought.get(this.currentUpgrade) - 1);

        for(let el of this.upgrades) {
          if (this.currentUpgrade == el.idUpgrade) {
            this.stats.nbSandwich += el.value * 50;
            this.nbSandwichPerClick -= el.value;
            this.nbSandwichPerSecond -= el.value / 4;
            this.checkNewAchievement();
          }
        }
      }
    }
  }

  checkNewAchievement(){
    const token = localStorage.getItem("token");
    if (token == null)
      return;

    let jwtDecode = require('jwt-decode');
    const tokenDecoded = jwtDecode(token);

    let found = false;

    for(let el of this.achievements){
      found = false;
      for(let elem of this.achieves){
        if(el.idAchievement == elem.idAchievement){
          found = true;
        }
      }
      if(!found){
        if(this.stats.nbSandwich >= el.value){
          let achieve = {
            idUser: +tokenDecoded.id_user,
            idAchievement: el.idAchievement,
            isAchieve: 1
          };
          this.subManager.addSub(this.achieveService.post(achieve).subscribe());
          this.achieves.push(achieve);
        }
      }
    }
  }
}
