import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchievementsGameComponent } from './achievements-game.component';

describe('AchievementsGameComponent', () => {
  let component: AchievementsGameComponent;
  let fixture: ComponentFixture<AchievementsGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchievementsGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchievementsGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
