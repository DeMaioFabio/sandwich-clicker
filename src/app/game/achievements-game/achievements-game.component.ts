import {Component, OnDestroy, OnInit} from '@angular/core';
import {AchievementsService} from "../../httpClientServices/achievements.service";
import {Achievement} from "../../models/achievement";
import {SubscriptionManagerService} from "../../util/subscription-manager.service";
import {Achieve} from "../../models/achieve";
import {AchieveService} from "../../httpClientServices/achieve.service";
import {JWTService} from "../../util/jwt.service";

@Component({
  selector: 'app-achievements-game',
  templateUrl: './achievements-game.component.html',
  styleUrls: ['./achievements-game.component.css']
})
export class AchievementsGameComponent implements OnInit, OnDestroy {

  public achievements: Achievement[] = [];
  private achieves: Achieve[] = [];

  constructor(private jwt: JWTService, private achievementsService: AchievementsService, private subManager: SubscriptionManagerService, private achieveService:AchieveService) { }

  ngOnInit() {
    this.loadAchievements();
    this.loadAchieves();
  }

  ngOnDestroy(): void {
    this.subManager.destroySub();
  }

  loadAchievements(){
    this.subManager.addSub(this.achievementsService.getAll().subscribe(response => this.achievements = response));
  }

  loadAchieves(){
    this.subManager.addSub(this.achieveService.getAll(this.jwt.getIdUser()).subscribe(response => this.achieves = response));
  }

  unlockedAchievement(el: Achievement) {
    for(let elem of this.achieves){
      if(elem.idAchievement == el.idAchievement){
        if(elem.isAchieve == 1){
          return true;
        }else{
          return false;
        }
      }
    }
    return false;
  }
}
