import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortUpgradeGame'
})
export class SortUpgradeGamePipe implements PipeTransform {

  transform(value: any[]): any {
    if(value == undefined) return value;

    value.sort((a, b) => {
      if(a.value < b.value){
        return -1;
      }else if(a.value > b.value){
        return 1;
      }else{
        return 0;
      }
    });
    return value;
  }

}
