import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'displayTimer'
})
export class DisplayTimerPipe implements PipeTransform {

  transform(value: any): any {
    if(value < 60){
      if(value < 10){
        return "00:0"+value;
      }
      return "00:"+value;
    }else if(value >= 60 && value < 3600){
      let minute = Math.trunc(value / 60);
      let second = value % 60;
      if(minute < 10 && second < 10){
        return "0"+minute+":0"+(value % 60);
      }else if(minute < 10 && second >= 10){
        return "0"+minute+":"+second;
      }else if(minute >= 10 && second < 10){
        return minute+":0"+second;
      }
      return minute+":"+second;
    }else if(value >= 3600 && value < 86400){
      let heure = Math.trunc(value / 3600);
      let minute = Math.trunc((value - (heure * 3600)) / 60);
      let second = value % 60;
      if(heure < 10 && minute < 10 && second < 10){
        return "0"+heure+":0"+minute+":0"+second;
      }else if(heure < 10 && minute < 10 && second >= 10){
        return "0"+heure+":0"+minute+":"+second;
      }
      else if(heure < 10 && minute >= 10 && second < 10){
        return "0"+heure+":"+minute+":0"+second;
      }else if(heure < 10 && minute >= 10 && second >= 10){
        return "0"+heure+":"+minute+":"+second;
      }else if(heure >= 10 && minute < 10 && second < 10){
        return ""+heure+":0"+minute+":0"+second;
      }else if(heure >= 10 && minute < 10 && second >= 10){
        return ""+heure+":0"+minute+":"+second;
      }else if(heure >= 10 && minute >= 10 && second < 10){
        return ""+heure+":"+minute+":0"+second;
      }
      return heure+":"+minute+":"+second;
    }
  }
}
