import {enableProdMode, TRANSLATIONS, TRANSLATIONS_FORMAT} from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import {LanguageService} from "./app/util/language.service";

if (environment.production) {
  enableProdMode();
}

declare const require;

if(localStorage.getItem("language") == null){
  localStorage.setItem("language", "en");
}

let language = localStorage.getItem("language");

const translations = require('raw-loader!./locales/messages.'+language+'.xlf').default;

platformBrowserDynamic().bootstrapModule(AppModule, {
  providers: [
    {provide: TRANSLATIONS, useValue: translations},
    {provide: TRANSLATIONS_FORMAT, useValue: 'xlf'}
  ]
});
